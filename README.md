# LAMMPS LATTICE DYNAMICS COMMANDS

## This repository contains the source and examples for two new LAMMPS commands

* dynamical_matrix
* third_order

dynamical_matrix calculates a dynamical matrix from the system by a finite difference routine

third_order calculates a third order tensor of the system by a finite difference routine
